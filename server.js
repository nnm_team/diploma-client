var http = require('http');
var mongoose = require('mongoose');

var db = require('./db');

var passwordEncrypting = require('./helpers/passwordEncrypting');

var config = require('./config');
var CONSTANTS = require('./constants');

var User = db.model(CONSTANTS.MODELS.USER);

db.on('error', function (err) {
    console.error('mongo connect error');
    console.error(err);
});

db.once('open', function () {
    console.log('open connection');

    var app = require('./app')(db);
    var server = http.createServer(app);

    require('./helpers/socket')(server);

    User.count({access: CONSTANTS.VERIFICATION.VERIFICATION_ADMIN}, function (err, count) {
        var user;
        var userModel;

        if (err) {
            return console.error(err);
        }

        if (count) {
            return;
        }

        user = {
            name          : 'Super Admin',
            mail          : 'admin@admin.com',
            phone         : '0501829394',
            password      : passwordEncrypting('12345678'),
            secretQuestion: '12345678',
            access        : CONSTANTS.VERIFICATION.VERIFICATION_ADMIN
        };

        userModel = new User(user);

        userModel.save(function (err) {
            err && console.error(err);
        });
    });

    server.listen(config.SERVER.PORT, function () {
        console.log('Start server:  ' + config.SERVER.URL);
    });
});
