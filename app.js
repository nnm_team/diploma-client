var express = require('express');
var bodyParser = require('body-parser');
var session = require('express-session');
var morgan = require('morgan');
var MongoStore = require('connect-mongo')(session);

var config = require('./config');

module.exports = function (db) {
    var app = express();

    var mongoStore = new MongoStore({
        mongooseConnection: db,
        autoReconnect     : true,
        ssl               : false,
        ttl               : 5 * 365 * 24 * 60 * 60 * 1000
    });
    var sessionOpts = {
        store            : mongoStore,
        secret           : config.SESSION_SECRET_KEY,
        resave           : false,
        saveUninitialized: true
    };

    app.use(express.static(__dirname + '/public'));
    app.use('/diary', express.static(__dirname + '/public/diary'));

    app.use(morgan('dev'));
    app.use(bodyParser.json({limit: '88mb'}));
    app.use(bodyParser.urlencoded({extended: true, limit: '88mb'}));
    app.use(session(sessionOpts));

    require('./routes')(app);

    return app;
};

