var crypto = require('crypto');

var config = require('../config');

module.exports = function (password) {
    return crypto
        .createHmac(config.CRYPTO.CODE, config.CRYPTO.SECRET)
        .update(password)
        .digest(config.CRYPTO.HASH);
};
