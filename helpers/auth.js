var CustomError = require('./error');

function AuthHelper() {
    
    this.isAuth = function (req, res, next) {
        var session = req.session;
        var loggedIn = session && session.account;

        if (loggedIn) {
            return next();
        }

        next(new CustomError({status: 401, message: 'User did not authorisation.'}));
    };

    this.destroy = function (req, res, next) {
        var session = req.session;

        session && session.destroy();

        next();
    }
    
}

module.exports = new AuthHelper();