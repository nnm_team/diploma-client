var nodemailer = require('nodemailer');

var config = require('../config');
var mailConfig = {
    host: config.EMAIL.HOST,
    port: config.EMAIL.PORT,
    auth: {
        user: config.EMAIL.MAIL,
        pass: config.EMAIL.PASS
    }
};

module.exports = function (email, subject, message, cb) {
    var smtpTransport = nodemailer.createTransport(mailConfig);
    var mailOptions = {
        from   : config.EMAIL.MAIL,
        to     : email,
        subject: subject,
        text   : message
    };

    smtpTransport.sendMail(mailOptions, function (err) {
        typeof cb === 'function' ?
            cb(err) : console.error(err);
    });
};