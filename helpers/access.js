var CONSTANTS = require('../constants');

var CustomError = require('./error');

function AccessHelper() {

    var accessError = new CustomError({name: 'AccessError', status: 403, message: 'Denied access.'});

    this.admin = function (req, res, next) {
        var userAccess = req.session.account.access;
        var access = userAccess & CONSTANTS.USER_TYPE.ADMIN;

        if (access) {
            return next();
        }

        next(accessError);
    };

    this.moderator = function (req, res, next) {
        var userAccess = req.session.account.access;
        var access = userAccess & CONSTANTS.USER_TYPE.MODERATOR;

        if (access) {
            return next();
        }

        next(accessError);
    };

    this.teacher = function (req, res, next) {
        var userAccess = req.session.account.access;
        var access = userAccess & CONSTANTS.USER_TYPE.TEACHER;

        if (access) {
            return next();
        }

        next(accessError);
    };

    this.student = function (req, res, next) {
        var userAccess = req.session.account.access;
        var access = userAccess & CONSTANTS.USER_TYPE.STUDENT;

        if (access) {
            return next();
        }

        next(accessError);
    };

    this.user = function (req, res, next) {
        var userAccess = req.session.account.access;
        var access = userAccess & CONSTANTS.USER_TYPE.USER;

        if (access) {
            return next();
        }

        next(accessError);
    };

}

module.exports = new AccessHelper();