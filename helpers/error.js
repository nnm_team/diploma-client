var defaultOpts = {
    status : 400,
    message: 'Bad request.',
    name   : 'BadRequest'
};

function CustomError(opts) {
    Error.captureStackTrace(this);

    opts || (opts = {});

    this.name = opts.name || defaultOpts.name;
    this.status = opts.status || defaultOpts.status;
    this.message = opts.message || defaultOpts.message;
}

CustomError.prototype = Object.create(Error.prototype);

module.exports = CustomError;