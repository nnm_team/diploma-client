var mongoose = require('mongoose');

var CONSTANTS = require('../constants');

module.exports = function (db) {
    var Schema = mongoose.Schema;
    var ObjectId = Schema.Types.ObjectId;

    var schema = new Schema({

        name: {
            type: String,
            require: true,
            minLength: 2,
            maxLength: 25
        },

        course: {
            type: Number,
            require: true,
            default: 1,
            min: 1,
            max: 6
        },

        studentList: [ObjectId]

    }, {collection: CONSTANTS.COLLECTIONS.GROUPS});

    db.model(CONSTANTS.MODELS.GROUP, schema);
};
