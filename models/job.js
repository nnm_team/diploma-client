var uuid = require('uuid');
var mongoose = require('mongoose');

var CONSTANTS = require('../constants');

module.exports = function (db) {
    var Schema = mongoose.Schema;
    var ObjectId = Schema.Types.ObjectId;
    var schema = new Schema({

        name: {
            type   : String,
            require: true
        },

        rating: {
            type   : Number,
            require: true,
            min    : 1,
            max    : 100
        },

        subject: {
            type: ObjectId,
            ref : 'Subject'
        },

        student: {
            type: ObjectId,
            ref : 'User'
        },

        teacher: {
            type: ObjectId,
            ref : 'User'
        },

        hash: {
            type    : String,
            required: true,
            default : uuid.v4
        }

    }, {collection: CONSTANTS.COLLECTIONS.JOBS});

    db.model(CONSTANTS.MODELS.JOB, schema);
};
