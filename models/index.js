module.exports = function (db) {
    require('./job')(db);
    require('./user')(db);
    require('./group')(db);
    require('./subject')(db);
};