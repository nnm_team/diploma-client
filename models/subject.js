var mongoose = require('mongoose');

var CONSTANTS = require('../constants');

module.exports = function (db) {
    var Schema = mongoose.Schema;
    var ObjectId = Schema.Types.ObjectId;

    var schema = new Schema({

        name: {
            type     : String,
            require  : true,
            minLength: 2,
            maxLength: 25
        },

        studentList: [ObjectId]

    }, {collection: CONSTANTS.COLLECTIONS.SUBJECTS});

    db.model(CONSTANTS.MODELS.SUBJECT, schema);
};
