var authHelper = require('../helpers/auth');
var accessHelper = require('../helpers/access');

const diary = require('../handlers/diary');
const teacherHandler = require('../handlers/teacher');

var jobRouter = require('./job');
var authRouter = require('./auth');
var groupRouter = require('./group');
var studentRouter = require('./student');
var teacherRouter = require('./teacher');
var subjectRouter = require('./subject');
var statisticsRouter = require('./statistics');
var verificationRouter = require('./verification');

function errorHandler(err, req, res, next) {
    if (!err) {
        return res.status(200).send({});
    }

    if (!err.status) {
        err.status = 500;
    }

    if (!err.message) {
        err.message = 'CustomError';
    }

    res.status(err.status).send({error: err.message, stack: err.stack});
}

module.exports = function (app) {
    app.get('/diary-info', diary);
    app.get('/validate/teacher', teacherHandler.validateTeacher);

    app.use('/auth', authRouter);
    app.use('/jobs', authHelper.isAuth, jobRouter);
    app.use('/groups', authHelper.isAuth, groupRouter);
    app.use('/teachers', authHelper.isAuth, teacherRouter);
    app.use('/students', authHelper.isAuth, studentRouter);
    app.use('/subjects', authHelper.isAuth, accessHelper.admin, subjectRouter);
    app.use('/statistics', authHelper.isAuth, accessHelper.teacher, statisticsRouter);
    app.use('/verification', authHelper.isAuth, accessHelper.admin, verificationRouter);

    app.use(errorHandler);
};