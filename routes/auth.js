var express = require('express');
var route = express.Router();

var multer = require('multer');
var upload = multer({dest: 'public/uploads/'});

var authHelper = require('../helpers/auth');

var authHandler = require('../handlers/auth');

route.get('/', authHelper.isAuth, authHandler.checkAuth);

route.post('/signup', upload.single('avatar'), authHandler.signUp);

route.post('/signin', authHandler.signIn);

route.delete('/signout', authHelper.destroy, authHandler.signOut);

route.patch('/forgotPassword', authHandler.forgotPassword);

module.exports = route;