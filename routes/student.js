var route = require('express').Router();

var accessHelper = require('../helpers/access');

var studentHandler = require('../handlers/student');

route.delete('/:id', accessHelper.admin, studentHandler.removeStudentFromGroup);

module.exports = route;