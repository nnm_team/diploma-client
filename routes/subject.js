var route = require('express').Router();

var subjectHandler = require('../handlers/subject');

//var deleteTeacherSubject = require('../handlers/user').deleteSubject;
//var deleteJobsOnSubjects = require('../handlers/job').deleteJobsOnSubjects;
//var deleteStudentJobs = require('../handlers/user').deleteStudentJobs;

route.get('/', subjectHandler.fetch);

route.post('/', subjectHandler.create);

route.patch('/:id', subjectHandler.update);

route.delete('/:id', subjectHandler.delete);

module.exports = route;