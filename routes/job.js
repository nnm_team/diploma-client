var express = require('express');
var route = express.Router();

var accessHelper = require('../helpers/access');

var jobHandler = require('../handlers/job');

route.post('/student/:id', accessHelper.teacher, jobHandler.createJob);

module.exports = route;