var route = require('express').Router();

var accessHelper = require('../helpers/access');

var groupHandler = require('../handlers/group');

route.get('/', accessHelper.user, groupHandler.fetch);

route.post('/', accessHelper.admin, groupHandler.create);

route.get('/:id', accessHelper.user, groupHandler.fetchById);

route.patch('/:id', accessHelper.admin, groupHandler.update);

route.delete('/:id', accessHelper.admin, groupHandler.delete);

module.exports = route;