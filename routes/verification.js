var express = require('express');
var route = express.Router();

var verificationHandler = require('../handlers/verification');

route.get('/', verificationHandler.verification);

route.patch('/:id/admin', verificationHandler.verificationAdmin);

route.patch('/:id/teacher', verificationHandler.verificationTeacher);

route.patch('/:id/student', verificationHandler.verificationStudent);

route.delete('/:id/delete', verificationHandler.delete);

module.exports = route;