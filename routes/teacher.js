var route = require('express').Router();

var accessHelper = require('../helpers/access');

var teacherHandler = require('../handlers/teacher');

route.get('/', accessHelper.user, teacherHandler.fetch);
route.get('/:id', accessHelper.user, teacherHandler.fetchById);
route.patch('/:id', accessHelper.admin, teacherHandler.update);
route.delete('/:id', accessHelper.admin, teacherHandler.delete);

module.exports = route;