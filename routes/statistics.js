var express = require('express');
var route = express.Router();

var statisticsHandler = require('../handlers/statistics');

route.get('/groups', statisticsHandler.getStatisticsGroups);
route.get('/courses', statisticsHandler.getStatisticsCourse);
route.get('/subjects', statisticsHandler.getStatisticsSubjects);

module.exports = route;