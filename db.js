var db;
var config = require('./config');
var mongoose = require('mongoose');

mongoose.Promise = global.Promise;

db = mongoose.createConnection(config.DB.HOST, config.DB.NAME, config.DB.PORT);

require('./models')(db);

module.exports = db;
