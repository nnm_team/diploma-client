module.exports = {

    COLLECTIONS: {
        JOBS    : 'Jobs',
        USERS   : 'Users',
        BLOGS   : 'Blogs',
        POSTS   : 'Posts',
        GROUPS  : 'Groups',
        COMMENTS: 'Comments',
        SUBJECTS: 'Subjects'
    },

    MODELS: {
        JOB    : 'Job',
        USER   : 'User',
        BLOG   : 'Blog',
        POST   : 'Post',
        GROUP  : 'Group',
        COMMENT: 'Comment',
        SUBJECT: 'Subject'
    },

    USER_TYPE: {
        USER     : 1,
        STUDENT  : 2,
        TEACHER  : 4,
        ADMIN    : 8,
        MODERATOR: 12
    },

    VERIFICATION: {
        NOT_VERIFICATION    : 0,
        VERIFICATION_STUDENT: 3,
        VERIFICATION_TEACHER: 5,
        VERIFICATION_ADMIN  : 9
    }

};
