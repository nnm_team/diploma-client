var uuid = require('uuid');
var async = require('async');
var _ = require('underscore');

var CONSTANTS = require('../constants');

var db = require('../db');
var User = db.model(CONSTANTS.MODELS.USER);

var groupService = require('../services/group');

var CustomError = require('../helpers/error');

function VerificationHandler() {

    this.verification = function (req, res, next) {
        User.find({access: CONSTANTS.VERIFICATION.NOT_VERIFICATION})
            .exec(function (err, users) {
                if (err) {
                    return next(err);
                }

                res.status(200).send(users);
            });
    };

    this.verificationAdmin = function (req, res, next) {
        var id = req.params.id;
        var update = {access: CONSTANTS.VERIFICATION.VERIFICATION_ADMIN};

        if (!id) {
            return next(new CustomError({message: 'Please pass all required fields.'}));
        }


        User.update({_id: id}, update)
            .exec(function (err) {
                if (err) {
                    return next(err);
                }

                res.status(200).send({message: 'Admin was verificated.'});
            });
    };

    this.verificationTeacher = function (req, res, next) {
        var teacherId = req.params.id;
        var body = req.body;
        var subjects = body.subjects;

        async.waterfall([

            function (cb) {
                User.findById(teacherId)
                    .exec(function (err, teacherModel) {
                        if (err) {
                            return cb(err);
                        }

                        if (!teacherModel) {
                            return cb(new CustomError({name: 'NotFound', status: 400, message: 'User did not find.'}));
                        }

                        cb(null, teacherModel);
                    });
            },

            function (teacherModel, cb) {
                teacherModel.set('teacherToken', uuid.v4());
                teacherModel.set('subjects', _.uniq(subjects));
                teacherModel.set('access', CONSTANTS.VERIFICATION.VERIFICATION_TEACHER);

                teacherModel.save(function (err) {
                    if (err) {
                        return cb(err);
                    }

                    cb(null);
                });
            }

        ], function (err) {
            if (err) {
                return next(err);
            }

            res.status(200).send({message: 'Teacher was verificated.'});
        });
    };

    this.verificationStudent = function (req, res, next) {
        var studentId = req.params.id;
        var body = req.body;
        var groupId = body.group;

        async.waterfall([

            function (cb) {
                User.findById(studentId)
                    .exec(function (err, studentModel) {
                        if (err) {
                            return cb(err);
                        }

                        if (!studentModel) {
                            return cb(new CustomError({name: 'NotFound', status: 400, message: 'User did not find.'}));
                        }

                        cb(null, studentModel);
                    });
            },

            function (studentModel, cb) {
                studentModel.set('group', groupId);
                studentModel.set('access', CONSTANTS.VERIFICATION.VERIFICATION_STUDENT);

                studentModel.save(function (err) {
                    if (err) {
                        return cb(err);
                    }

                    cb(null);
                });
            },

            function (cb) {
                groupService.addToGroup(groupId, studentId, cb);
            }

        ], function (err) {
            if (err) {
                return next(err);
            }

            res.status(200).send({message: 'Student was verificated.'});
        });


    };

    this.delete = function (req, res, next) {
        var id = req.params.id;

        if (!id) {
            return next(new CustomError({message: 'Please pass all required fields.'}));
        }

        User.remove({_id: id})
            .exec(function (err) {
                if (err) {
                    return next(err);
                }

                res.status(200).send({message: 'User was deleted.'});
            });
    };

}

module.exports = new VerificationHandler();
