var async = require('async');

var CONSTANTS = require('../constants');

var db = require('../db');
var Job = db.model(CONSTANTS.MODELS.JOB);
var User = db.model(CONSTANTS.MODELS.USER);
var Subject = db.model(CONSTANTS.MODELS.SUBJECT);

//var aggregateQuery = require('../config/query');
var ObjectId = require('mongodb').ObjectID;

var CustomError = require('../helpers/error');

function SubjectHandler() {

    this.fetch = function (req, res, next) {
        var project = {
            name: 1
        };

        Subject.find({}, project)
            .exec(function (err, subjects) {
                if (err) {
                    return next(err);
                }

                res.status(200).send(subjects);
            });
    };

    this.create = function (req, res, next) {
        var subject;
        var body = req.body;
        var name = body.name;

        if (!name) {
            return next(new CustomError({message: 'Please type all required fields.'}));
        }

        subject = {name: name};

        async.waterfall([

            function (cb) {
                Subject.findOne(subject)
                    .exec(function (err, subjectModel) {
                        if (err) {
                            return cb(err);
                        }

                        if (subjectModel) {
                            return next(new CustomError({message: 'Subject already exists.'}));
                        }

                        cb(null);
                    });
            },

            function (cb) {
                var subjectModel = new Subject(subject);

                subjectModel.save(function (err) {
                    if (err) {
                        return cb(err);
                    }

                    cb(null);
                });
            }

        ], function (err) {
            if (err) {
                return next(err);
            }

            res.status(200).send({message: 'Subject was create successfully.'});
        });
    };

    this.update = function (req, res, next) {
        var update = {};
        var body = req.body;
        var name = body.name;
        var subjectId = req.params.id;

        if (name) {
            update.name = name;
        }

        async.waterfall([

            function (cb) {
                Subject.findById(subjectId)
                    .exec(function (err, subjectModel) {
                        if (err) {
                            return cb(err);
                        }

                        if (!subjectModel) {
                            return cb(new CustomError({name: 'NotFound', status: 404, message: 'Subject not found.'}));
                        }

                        cb(null);
                    });
            },

            function (cb) {
                Subject.findByIdAndUpdate(subjectId, {$set: update}).exec(cb);
            }

        ], function (err) {
            if (err) {
                return next(err)
            }

            res.status(200).send({message: 'Subject was update successfully.'});
        });
    };

    this.delete = function (req, res, next) {
        var subjectId = req.params.id;

        async.waterfall([

            function (wCb) {
                async.parallel([

                    function (pCb) {
                        var update = {$pull: {subjects: subjectId}};

                        User.update({}, update, {multi: true}).exec(pCb);
                    },

                    function (pCb) {
                        var query = {
                            subject: subjectId
                        };

                        async.waterfall([

                            function (pWcb) {
                                Job.find(query)
                                    .lean()
                                    .exec(function (err, jobs) {
                                        var jobIds;

                                        if (err) {
                                            return pWcb(err);
                                        }

                                        jobIds = jobs.map(function (job) {
                                            return job._id;
                                        });

                                        pWcb(null, jobIds);
                                    });
                            },

                            function (jobIds, pWcb) {
                                var update = {
                                    $pull: {
                                        jobs: {$in: jobIds}
                                    }
                                };

                                User.update({}, update, {multi: true})
                                    .exec(function (err) {
                                        pWcb(err);
                                    });
                            },

                            function (pWcb) {
                                Job.remove(query).exec(function (err) {
                                    pWcb(err);
                                });
                            }

                        ], function (err) {
                            pCb(err);
                        });
                    }

                ], function (err) {
                    wCb(err);
                });
            },

            function (wCb) {
                Subject.remove({_id: subjectId}).exec(function (err) {
                    wCb(err);
                });
            }

        ], function (err) {
            if (err) {
                return next(err);
            }

            res.status(200).send({message: 'Subject was remove successfully.'});
        });
    };

//    this.addToGroup = function (req, res, next) {
//        var _id = req.body.subject;
//        var student = req.body.student;
//        var update = {$addToSet: {studentList: student}};
//
//        Subject.findByIdAndUpdate(_id, update, {new: true})
//            .exec(function (err, subject) {
//                if (err)
//                    return next(new DataBaseError());
//
//                req.body.subject = subject.name;
//                next();
//            });
//    };
//
//    this.deleteStudent = function (req, res, next) {
//        var _id = req.params.id;
//
//        var query = {};
//        var update = {$pull: {studentList: _id}};
//        var options = {multi: true};
//
//        Subject.update(query, update, options)
//            .exec(function (err, update) {
//                if (err)
//                    return next(new DataBaseError());
//
//                next();
//            });
//    };

}

module.exports = new SubjectHandler();