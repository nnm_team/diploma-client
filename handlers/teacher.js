var async = require('async');

var CONSTANTS = require('../constants');

var db = require('../db');
var User = db.model(CONSTANTS.MODELS.USER);

var teacherService = require('../services/teacher');

var ObjectId = require('mongodb').ObjectID;

var CustomError = require('../helpers/error');

function TeacherHandler() {

    this.fetch = function (req, res, next) {
        var aggregateParams = [];

        aggregateParams.push(
            {
                $match: {
                    access: CONSTANTS.VERIFICATION.VERIFICATION_TEACHER
                }
            },
            // {
            //     $unwind: '$subjects'
            // },
            {
                $lookup: {
                    from        : CONSTANTS.COLLECTIONS.SUBJECTS,
                    localField  : 'subjects',
                    foreignField: '_id',
                    as          : 'subjects'
                }
            },
            // {
            //     $project: {
            //         name    : 1,
            //         avatar  : 1,
            //         subjects: {
            //             $arrayElemAt: ['$subjects', 0]
            //         }
            //     }
            // },
            {
                $project: {
                    _id     : 1,
                    name    : 1,
                    avatar  : 1,
                    subjects: {
                        _id : 1,
                        name: 1
                    }
                }
            },
            // {
            //     $group: {
            //         _id     : '$_id',
            //         avatar  : {$first: '$avatar'},
            //         name    : {$first: '$name'},
            //         subjects: {$addToSet: '$subjects'}
            //     }
            // }
        );

        User.aggregate(aggregateParams, function (err, teachers) {
            if (err) {
                return next(err);
            }

            res.status(200).send(teachers);
        });
    };

    this.fetchById = function (req, res, next) {
        var aggregateParams = [];
        var teacherId = req.params.id;

        aggregateParams.push(
            {
                $match: {
                    _id   : new ObjectId(teacherId),
                    access: 5
                }
            },
            {
                $unwind: '$subjects'
            },
            {
                $lookup: {
                    from        : 'Subjects',
                    localField  : 'subjects',
                    foreignField: '_id',
                    as          : 'subjects'
                }
            },
            {
                $project: {
                    name    : 1,
                    avatar  : 1,
                    subjects: {
                        $arrayElemAt: ['$subjects', 0]
                    }
                }
            },
            {
                $project: {
                    name    : 1,
                    avatar  : 1,
                    subjects: {
                        _id : 1,
                        name: 1
                    }
                }
            },
            {
                $group: {
                    _id     : '$_id',
                    avatar  : {$first: '$avatar'},
                    name    : {$first: '$name'},
                    subjects: {$addToSet: '$subjects'}
                }
            }
        );

        User.aggregate(aggregateParams, function (err, teacher) {
            if (err) {
                return next(err);
            }

            if (!teacher) {
                return next(new CustomError({message: 'Teacher not found.'}));
            }

            teacher = teacher[0];

            res.status(200).send(teacher);
        });
    };

    this.update = function (req, res, next) {
        var teacherId = req.params.id;
        var body = req.body;
        var subjects = body.subjects;

        teacherService.addSubjects(teacherId, subjects, function (err) {
            if (err) {
                return next(err);
            }

            res.status(200).send({message: 'Subjects was updated.'});
        });
    };

    this.delete = function (req, res, next) {
        var teacherId = ObjectId(req.params.id);

        teacherService.delete(teacherId, function (err) {
            if (err) {
                return next(err);
            }

            res.status(200).send({message: 'Teacher was deleted.'});
        });
    };

    this.validateTeacher = async function (req, res, next) {
        try {
            let valid = false;
            const {token: teacherToken} = req.query;

            if (!teacherToken) {
                res.status(200).json({valid});
            }

            const teacherModel = await User.findOne({teacherToken});

            if (teacherModel) {
                valid = true;
            }

            res.status(200).json({valid});
        } catch (err) {
            next(err);
        }
    };

}

module.exports = new TeacherHandler();