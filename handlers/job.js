var async = require('async');
var request = require('request-promise');

var CONSTANTS = require('../constants');

var db = require('../db');
var Job = db.model(CONSTANTS.MODELS.JOB);
var User = db.model(CONSTANTS.MODELS.USER);
var Group = db.model(CONSTANTS.MODELS.GROUP);
var Subject = db.model(CONSTANTS.MODELS.SUBJECT);

var ObjectId = require('mongodb').ObjectID;

const config = require('../config');

var CustomError = require('../helpers/error');

function JobHandler() {

    this.createJob = async function (req, res, next) {
        try {
            const {
                params : {
                    id: studentId
                },
                body   : {
                    name,
                    rating,
                    subject
                },
                session: {
                    account: {
                        _id: teacherId
                    }
                }
            } = req;

            if (!name || !rating || !subject) {
                return next(new CustomError({message: 'Please pass all required fields.'}));
            }

            if (rating < 1 || rating > 100) {
                return next(new CustomError({message: 'Incorrect rating.'}));
            }

            const [teacherModel, studentModel] = await Promise.all([
                User.findOne({_id: teacherId, access: CONSTANTS.VERIFICATION.VERIFICATION_TEACHER}),
                User.findOne({_id: studentId, access: CONSTANTS.VERIFICATION.VERIFICATION_STUDENT})
            ]);

            if (!teacherModel || !studentModel) {
                throw new CustomError('User not found');
            }

            const body = {
                subject,
                job          : name,
                amount       : rating,
                teacher      : teacherId,
                student      : studentId,
                created_at   : Date.now(),
                teacher_token: teacherModel.get('teacherToken')
            };

            const opts = {
                body,
                json: true,
                uri : `${config.BLOCKCHAIN.MAIN_PEER}/transaction`
            };

            await request.post(opts);

            console.log(`${config.BLOCKCHAIN.MAIN_PEER} success create transaction`);

            res.status(200).send({message: 'Job successfully created.'});
        } catch (err) {
            next(err);
        }
    };

    this._createJob = function (req, res, next) {
        var body = req.body;
        var studentId = req.params.id;
        var teacherId = req.session.account._id;
        var name = body.name;
        var rating = body.rating;
        var subject = body.subject;

        if (!name || !rating || !subject) {
            return next(new CustomError({message: 'Please pass all required fields.'}));
        }

        if (rating < 1 || rating > 100) {
            return next(new CustomError({message: 'Incorrect rating.'}));
        }

        async.waterfall([

            function (wCb) {
                var job = {
                    name   : name,
                    rating : rating,
                    subject: subject,
                    teacher: teacherId,
                    student: studentId
                };
                var jobModel = new Job(job);

                jobModel.save(function (err, job) {
                    if (err) {
                        return wCb(err);
                    }

                    wCb(null, job.get('_id'));
                });
            },

            function (jobId, wCb) {

                async.parallel([

                    function (pCb) {
                        var update = {$addToSet: {jobs: jobId}};

                        User.update({_id: studentId}, update).exec(pCb);
                    },

                    function (pCb) {
                        var update = {$addToSet: {studentList: studentId}};

                        Subject.update({_id: subject}, update).exec(pCb);
                    }

                ], wCb);

            }

        ], function (err) {
            if (err) {
                return next(err);
            }

            res.status(200).send({message: 'Job successfully created.'});
        })
    };

}

module.exports = new JobHandler();
