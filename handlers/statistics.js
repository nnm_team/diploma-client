var async = require('async');
var mongoose = require('mongoose');

var CONSTANTS = require('../constants');

var db = require('../db');
var User = db.model(CONSTANTS.MODELS.USER);
var Group = db.model(CONSTANTS.MODELS.GROUP);
var Subject = db.model(CONSTANTS.MODELS.SUBJECT);

var ObjectId = mongoose.Types.ObjectId;

function StatisticsHandler() {
    this.getStatisticsGroups = function (req, res, next) {
        var aggregateParams = [];

        aggregateParams.push(
            {
                $unwind: '$studentList'
            },
            {
                $lookup: {
                    from        : 'Users',
                    localField  : 'studentList',
                    foreignField: '_id',
                    as          : 'studentList'
                }
            },
            {
                $project: {
                    name   : 1,
                    course : 1,
                    student: {$arrayElemAt: ['$studentList', 0]}
                }
            },
            {
                $project: {
                    name  : 1,
                    course: 1,
                    jobs  : '$student.jobs'
                }
            },
            {
                $unwind: '$jobs'
            },
            {
                $lookup: {
                    from        : 'Jobs',
                    localField  : 'jobs',
                    foreignField: '_id',
                    as          : 'job'
                }
            },
            {
                $project: {
                    name  : 1,
                    course: 1,
                    job   : {$arrayElemAt: ['$job', 0]}
                }
            },
            {
                $project: {
                    name  : 1,
                    course: 1,
                    rating: '$job.rating'
                }
            },
            {
                $group: {
                    _id   : '$_id',
                    name  : {$first: '$name'},
                    course: {$first: '$course'},
                    avg   : {$avg: '$rating'}
                }
            }
        );

        Group.aggregate(aggregateParams, function (err, statistics) {
            if (err) {
                return next(err);
            }

            res.status(200).send(statistics);
        });
    };
    this.getStatisticsCourse = function (req, res, next) {
        var aggregateParams = [];

        aggregateParams.push(
            {
                $unwind: '$studentList'
            },
            {
                $lookup: {
                    from        : 'Users',
                    localField  : 'studentList',
                    foreignField: '_id',
                    as          : 'studentList'
                }
            },
            {
                $project: {
                    name   : 1,
                    course : 1,
                    student: {$arrayElemAt: ['$studentList', 0]}
                }
            },
            {
                $project: {
                    name  : 1,
                    course: 1,
                    jobs  : '$student.jobs'
                }
            },
            {
                $unwind: '$jobs'
            },
            {
                $lookup: {
                    from        : 'Jobs',
                    localField  : 'jobs',
                    foreignField: '_id',
                    as          : 'job'
                }
            },
            {
                $project: {
                    name  : 1,
                    course: 1,
                    job   : {$arrayElemAt: ['$job', 0]}
                }
            },
            {
                $project: {
                    _id   : 0,
                    name  : 1,
                    course: 1,
                    rating: '$job.rating'
                }
            },
            {
                $group: {
                    _id : '$course',
                    name: {$first: '$name'},
                    avg : {$avg: '$rating'}
                }
            },
            {
                $project: {
                    _id : 0,
                    name: '$_id',
                    avg : 1
                }
            }
        );

        Group.aggregate(aggregateParams, function (err, statistics) {
            if (err) {
                return next(err);
            }

            res.status(200).send(statistics);
        });
    };
    this.getStatisticsSubjects = function (req, res, next) {
        var aggregateParams = [];

        aggregateParams.push(
            {
                $unwind: '$studentList'
            },
            {
                $lookup: {
                    from        : 'Users',
                    localField  : 'studentList',
                    foreignField: '_id',
                    as          : 'studentList'
                }
            },
            {
                $project: {
                    name   : 1,
                    student: {$arrayElemAt: ['$studentList', 0]}
                }
            },
            {
                $project: {
                    name: 1,
                    jobs: '$student.jobs'
                }
            },
            {
                $unwind: '$jobs'
            },
            {
                $lookup: {
                    from        : 'Jobs',
                    localField  : 'jobs',
                    foreignField: '_id',
                    as          : 'job'
                }
            },
            {
                $project: {
                    name: 1,
                    job : {$arrayElemAt: ['$job', 0]}
                }
            },
            {
                $project: {
                    _id   : 0,
                    name  : 1,
                    rating: '$job.rating'
                }
            },
            {
                $group: {
                    _id: '$name',
                    avg: {$avg: '$rating'}
                }
            },
            {
                $project: {
                    _id : 0,
                    name: '$_id',
                    avg : 1
                }
            }
        );

        Subject.aggregate(aggregateParams, function (err, statistics) {
            if (err) {
                return next(err);
            }

            res.status(200).send(statistics);
        });
    };
}

module.exports = new StatisticsHandler();