var async = require('async');

var CONSTANTS = require('../constants');

var db = require('../db');
var User = db.model(CONSTANTS.MODELS.USER);

var mailHelper = require('../helpers/mail');
var passwordGenerator = require('../helpers/generatePassword');
var passwordEncryption = require('../helpers/passwordEncrypting');

var CustomError = require('../helpers/error');

function AuthHandler() {

    var passwordConfig = {
        length : 10,
        numbers: true
    };

    this.checkAuth = function (req, res, next) {
        res.status(200).send(req.session.account);
    };

    this.signUp = function (req, res, next) {
        var body = req.body;
        var name = body.name;
        var date = body.date;
        var mail = body.mail;
        var phone = body.phone;
        var password = body.password;
        var secretQuestion = body.secretQuestion;
        var avatar = req.file && req.file.filename;

        if (!name || !mail || !password || !secretQuestion) {
            return next(new CustomError({message: 'Type all required fields!'}));
        }

        if (password.length < 6) {
            return next(new CustomError({message: 'Incorrect password!'}));
        }

        async.waterfall([

            function (cb) {
                User.findOne({mail: mail}, function (err, model) {
                    if (err) {
                        return cb(err);
                    }

                    if (model) {
                        return cb(new CustomError({message: 'Email already was used.'}));
                    }

                    cb(null);
                })
            },

            function (cb) {
                var userModel;
                var user = {
                    access        : 0,
                    name          : name,
                    date          : date,
                    mail          : mail,
                    phone         : phone,
                    secretQuestion: secretQuestion,
                    password      : passwordEncryption(password)
                };

                avatar && (user.avatar = avatar);

                userModel = new User(user);

                userModel.save(function (err) {
                    if (err) {
                        return cb(err);
                    }

                    cb(null);
                });
            }

        ], function (err) {
            if (err) {
                return next(err);
            }

            res.status(200).send({message: 'User was sign up successfully.'});
        });
    };

    this.signIn = function (req, res, next) {
        var query;
        var project;
        var body = req.body;
        var mail = body.mail;
        var password = body.password;

        if (!mail || !password) {
            return next(new CustomError({message: 'Mail and password is required.'}));
        }

        query = {
            mail    : mail,
            password: passwordEncryption(password)
        };

        project = {
            name        : 1,
            mail        : 1,
            access      : 1,
            group       : 1,
            jobs        : 1,
            subjects    : 1,
            avatar      : 1,
            blogs       : 1,
            subscription: 1,
            notification: 1,
            teacherToken: 1
        };

        User.findOne(query, project)
            .lean()
            .exec(function (err, user) {
                var session;

                if (err) {
                    return next(err);
                }

                if (!user) {
                    return next(new CustomError({name: 'AuthError', status: 401, message: 'Auth error.'}));
                }

                session = req.session;

                session.account = user;

                res.status(200).send(user);
            });
    };

    this.signOut = function (req, res, next) {
        res.status(200).send({message: 'User sign out.'});
    };

    this.forgotPassword = function (req, res, next) {
        var query;
        var body = req.body;
        var mail = body.mail;
        var secretQuestion = body.secretQuestion;

        if (!mail || !secretQuestion) {
            return next(new CustomError({message: 'Mail and secret question is required.'}));
        }

        query = {
            mail          : mail,
            secretQuestion: secretQuestion
        };

        async.waterfall([

            function (cb) {
                User.findOne(query)
                    .exec(function (err, userModel) {
                        if (err) {
                            return cb(err);
                        }

                        if (!userModel) {
                            return next(new CustomError({
                                name   : 'NotFound',
                                status : 404,
                                message: 'User did not find.'
                            }));
                        }

                        cb(null);
                    });
            },

            function (cb) {
                var password = passwordGenerator(passwordConfig);
                var update = {
                    password: passwordEncryption(password)
                };

                User.update(query, update)
                    .exec(function (err) {
                        if (err) {
                            return next(err);
                        }

                        cb(null, password);
                    });
            }

        ], function (err, password) {
            if (err) {
                return next(err);
            }

            res.status(200).send({message: 'Password changed.'});

            mailHelper(mail, 'Password was changed!', password);
        });
    };

}

module.exports = new AuthHandler();