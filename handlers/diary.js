const _ = require('lodash');
const moment = require('moment');
const request = require('request-promise');

const CONSTANTS = require('../constants');

const db = require('../db');
const User = db.model(CONSTANTS.MODELS.USER);
const Subject = db.model(CONSTANTS.MODELS.SUBJECT);

const config = require('../config');

const template = `
<style>
    td {
        padding: 10px;
        text-align: center;
    }
    th {
        text-align: center;
    }
</style>

<%Object.keys(hashList).forEach(studentId => {%>
<hr>
<h2><%=hashList[studentId].student.name%></h2>
<table border="1">
<tr>
<th>Subject</th>
<th>Job name</th>
<th>Created at</th>
<th>Teacher</th>
<th>Rating</th>
</tr>
<%hashList[studentId].transactions.forEach(transaction => {%>
<tr>
<td><%=transaction.subject.name%></td>
<td><%=transaction.job%></td>
<td><%=transaction.createdAt%></td>
<td><%=transaction.teacher.name%></td>
<td><%=transaction.amount%></td>
</tr>
<%});%>
</table>
<%});%>
<hr>
`;

module.exports = async function (req, res, next) {
    try {
        const response = await request({uri: `${config.BLOCKCHAIN.MAIN_PEER}/chain`});

        const blocks = JSON.parse(response);

        const usersIds = [];
        const subjectsIds = [];
        const transactions = [];
        const studentsHashList = {};

        blocks.forEach(block => {
            block.transactions.forEach(transaction => {
                const {teacher, student, subject, job, amount, created_at: createdAt} = transaction;

                if (!usersIds.includes(teacher)) {
                    usersIds.push(teacher);
                }

                if (!usersIds.includes(student)) {
                    usersIds.push(student);
                }

                if (!subjectsIds.includes(subject)) {
                    subjectsIds.push(subject);
                }

                const transactionDocument = {
                    job,
                    amount,
                    subject,
                    teacher,
                    student,
                    createdAt: moment(+createdAt).format('DD MMM YYYY | HH[h]mm[m]')
                };

                transactions.push(transactionDocument);
            });
        });

        const [users, subjects] = await Promise.all([
            await User.find({_id: {$in: usersIds}}).lean(),
            await Subject.find({_id: {$in: subjectsIds}}).lean()
        ]);

        transactions.forEach(transaction => {
            let foundUsers = 0;

            for (const user of users) {
                if (user._id.toString() === transaction.student) {
                    transaction.student = user;

                    if (++foundUsers === 2) {
                        break;
                    }
                }

                if (user._id.toString() === transaction.teacher) {
                    transaction.teacher = user;

                    if (++foundUsers === 2) {
                        break;
                    }
                }
            }

            for (const subject of subjects) {
                if (subject._id.toString() === transaction.subject) {
                    transaction.subject = subject;
                    break;
                }
            }
        });

        transactions.forEach(transaction => {
            if (studentsHashList[transaction.student._id.toString()]) {
                studentsHashList[transaction.student._id.toString()].transactions.push(transaction);
            } else {
                studentsHashList[transaction.student._id.toString()] = {
                    transactions: [transaction],
                    student     : transaction.student
                };
            }

        });

        res.status(200).send({hashList: studentsHashList});
    } catch (err) {
        next(err);
    }
};
