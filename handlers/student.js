var async = require('async');

var CONSTANTS = require('../constants');

var db = require('../db');
var User = db.model(CONSTANTS.MODELS.USER);
var Group = db.model(CONSTANTS.MODELS.GROUP);

var ObjectId = require('mongodb').ObjectID;

var CustomError = require('../helpers/error');

function StudentHandler() {

    this.removeStudentFromGroup = function (req, res, next) {
        var studentId = req.params.id;

        async.waterfall([

            function (wCb) {
                User.findById(studentId, function (err, studentModel) {
                    var groupId;

                    if (err) {
                        return wCb(err);
                    }

                    groupId = studentModel.get('group');

                    if (!groupId) {
                        return wCb(new CustomError({message: 'Student do not have a group.'}));
                    }

                    wCb(null, groupId);
                });
            },

            function (groupId, wCb) {

                async.parallel([

                    function (pCb) {
                        var query = {_id: groupId};
                        var update = {$pull: {studentList: studentId}};

                        Group.update(query, update)
                            .exec(function (err) {
                                if (err) {
                                    return pCb(err);
                                }

                                pCb(null);
                            });
                    },

                    function (pCb) {
                        var query = {_id: studentId};
                        var update = {group: null};

                        User.update(query, update)
                            .exec(function (err) {
                                if(err){
                                    return pCb(err);
                                }

                                pCb(null);
                            });
                    }

                ], wCb);

            }

        ], function (err) {
            if (err) {
                return next(err);
            }

            res.status(200).send({message: 'Student was removed from group.'});
        });
    };

}

module.exports = new StudentHandler();
