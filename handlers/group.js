var async = require('async');
var mongoose = require('mongoose');

var CONSTANTS = require('../constants');

var db = require('../db');
var User = db.model(CONSTANTS.MODELS.USER);
var Group = db.model(CONSTANTS.MODELS.GROUP);

var ObjectId = mongoose.Types.ObjectId;

var CustomError = require('../helpers/error');

function GroupHandler() {

    this.fetch = function (req, res, next) {
        var project = {
            name  : 1,
            course: 1
        };

        Group.find({}, project)
            .exec(function (err, groups) {
                if (err) {
                    return next(err);
                }

                res.status(200).send(groups);
            });
    };

    this.fetchById = function (req, res, next) {
        var id = req.params.id;
        var aggregateParams = [];

        if (!id) {
            return next(new CustomError({message: 'Please pass all required fields.'}));
        }

        aggregateParams.push(
            {
                $match: {
                    _id: new ObjectId(id)
                }
            },
            {
                $unwind: {
                    path                      : '$studentList',
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $lookup: {
                    from        : 'Users',
                    localField  : 'studentList',
                    foreignField: '_id',
                    as          : 'student'
                }
            },
            {
                $project: {
                    name   : 1,
                    course : 1,
                    student: {
                        $arrayElemAt: ['$student', 0]
                    }
                }
            },
            {
                $project: {
                    name   : 1,
                    course : 1,
                    student: {
                        _id   : 1,
                        avatar: 1,
                        name  : 1,
                        date  : 1
                    }
                }
            },
            {
                $group: {
                    _id     : '$_id',
                    name    : {$first: '$name'},
                    course  : {$first: '$course'},
                    students: {$addToSet: '$student'}
                }
            }
        );

        Group.aggregate(aggregateParams, function (err, group) {
            if (err) {
                return next(err);
            }

            if (!group.length) {
                return next(new CustomError({message: 'Group did not find.'}));
            }

            group = group[0];

            res.status(200).send(group);
        });
    };

    this.create = function (req, res, next) {
        var group;
        var body = req.body;
        var name = body.name;
        var course = body.course;

        if (!name || !course) {
            return next(new CustomError({message: 'Please type all required fields.'}));
        }

        if (course < 1 || course > 6) {
            return next(new CustomError({message: 'Incorrect course number.'}));
        }

        group = {
            name  : name,
            course: course
        };

        async.waterfall([

            function (cb) {
                Group.findOne(group)
                    .exec(function (err, groupModel) {
                        if (err) {
                            return cb(err);
                        }

                        if (groupModel) {
                            return next(new CustomError({message: 'Group already exists.'}));
                        }

                        cb(null);
                    });
            },

            function (cb) {
                var groupModel = new Group(group);

                groupModel.save(function (err) {
                    if (err) {
                        return cb(err);
                    }

                    cb(null);
                });
            }

        ], function (err) {
            if (err) {
                return next(err);
            }

            res.status(200).send({message: 'Group was create successfully.'});
        });
    };

    this.update = function (req, res, next) {
        var update = {};
        var body = req.body;
        var name = body.name;
        var course = body.course;
        var groupId = req.params.id;

        if (name) {
            update.name = name;
        }

        if (course && course >= 1 && course <= 6) {
            update.course = course;
        }

        async.waterfall([

            function (cb) {
                Group.findById(groupId)
                    .exec(function (err, groupModel) {
                        if (err) {
                            return cb(err);
                        }

                        if (!groupModel) {
                            return cb(new CustomError({name: 'NotFound', status: 404, message: 'Group not found.'}));
                        }

                        cb(null);
                    });
            },

            function (cb) {
                Group.findByIdAndUpdate(groupId, {$set: update}).exec(cb);
            }

        ], function (err) {
            if (err) {
                return next(err)
            }

            res.status(200).send({message: 'Group was update successfully.'});
        });


    };

    this.delete = function (req, res, next) {
        var groupId = req.params.id;

        async.waterfall([

            function (wCb) {
                Group.findById(groupId)
                    .exec(function (err, groupModel) {
                        var students = groupModel.get('studentList');

                        if (err) {
                            return wCb(null);
                        }

                        if (!groupModel) {
                            return wCb(null, null, null);
                        }

                        wCb(null, true, students);
                    });
            },

            function (exists, students, wCb) {
                if (!exists && (!students || students.length === 0)) {
                    return wCb(null);
                }

                async.parallel([

                    function (pCb) {
                        Group.remove({_id: groupId})
                            .exec(pCb);
                    },

                    function (pCb) {
                        var query = {group: groupId};
                        var update = {group: null};
                        var multi = {multi: true};

                        User.update(query, update, multi)
                            .exec(pCb);
                    }

                ], function (err) {
                    if (err) {
                        return wCb(err);
                    }

                    wCb(null);
                });
            }

        ], function (err) {
            if (err) {
                return next(err);
            }

            res.status(200).send({message: 'Group was removed.'});
        });
    };

}

module.exports = new GroupHandler();