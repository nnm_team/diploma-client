var CONSTANTS = require('../constants');

var db = require('../db');
var Group = db.model(CONSTANTS.MODELS.GROUP);

function GroupHelper() {

    this.addToGroup = function (groupId, studentId, cb) {
        var update = {
            $addToSet: {
                studentList: studentId
            }
        };

        Group.findByIdAndUpdate(groupId, update, {new: true})
            .exec(function (err) {
                typeof cb === 'function' && cb(err);
            });
    };

//    this.deleteStudentFromGroup = function (req, res, next) {
//        var data = req.body;
//
//        var _id = data.groupID;
//        var studentID = req.params.id;
//        var update = {$pull: {studentList: studentID}};
//
//        Group.findByIdAndUpdate(_id, update)
//            .exec(function (err, update) {
//                if (err)
//                    return next(new DataBaseError());
//
//                return next();
//            });
//    };

}

module.exports = new GroupHelper();
