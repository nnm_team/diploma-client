var async = require('async');

var CONSTANTS = require('../constants');

var db = require('../db');
var User = db.model(CONSTANTS.MODELS.USER);

var ObjectId = require('mongodb').ObjectID;

var CustomError = require('../helpers/error');

function TeacherService() {

    this.addSubjects = function (teacherId, subjects, cb) {
        User.findByIdAndUpdate(teacherId, {$set: {subjects: subjects}}).exec(cb);
    };

    this.delete = function (teacherId, cb) {
        User.findByIdAndUpdate(teacherId, {access: CONSTANTS.USER_TYPE.USER}).exec(cb);
    };

}

module.exports = new TeacherService();