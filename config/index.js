module.exports = {
    DB: {
        HOST: 'localhost',
        NAME: 'sms',
        PORT: 27017
    },

    BLOCKCHAIN: {
        MAIN_PEER: 'http://127.0.0.1:4041'
    },

    SERVER: {
        HOST: 'http://127.0.0.1',
        PORT: 3030,
        URL : 'http://127.0.0.1:3030'
    },

    SESSION_SECRET_KEY: 'efdfisdot48578jekfhdusie',

    CRYPTO: {
        HASH  : 'hex',
        CODE  : 'sha256',
        SECRET: 'crypto_secret'
    },

    EMAIL: {
        HOST: 'smtp.gmail.com',
        PORT: 465,
        MAIL: 'ptm.team.mail@gmail.com',
        PASS: 'Kf4jJisa'
    }
};
