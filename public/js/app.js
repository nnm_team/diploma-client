define(['jQuery', 'router'], function ($, Router) {

    window.APP = {};

    return function () {
        var APP = window.APP;

        $.ajax({
            method : 'GET',
            url    : 'auth',
            success: function (xhr) {
                APP.user = xhr;
                new Router();
            },
            error  : function () {
                new Router();
            }
        });
    };
});