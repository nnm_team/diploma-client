define(['Backbone', 'models/teacher'], function (Backbone, TeacherModel) {
    return Backbone.Collection.extend({

        model: TeacherModel,

        url: 'teachers'

    });
});