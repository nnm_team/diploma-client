define(['Backbone', 'models/subject'], function (Backbone, SubjectModel) {
    return Backbone.Collection.extend({

        model: SubjectModel,

        url: 'subjects'

    });
});