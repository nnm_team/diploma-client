define(['Backbone', '../models/group'],
    function (Backbone, GroupModel) {

        return Backbone.Collection.extend({
            idAttribute: '_id',

            url  : 'groups',

            model: GroupModel
        });

    });