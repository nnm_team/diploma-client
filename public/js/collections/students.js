define(['Backbone', 'models/student'],
    function (Backbone, StudentModel) {

        return Backbone.Collection.extend({

            model: StudentModel,

            url: 'students'

        });

    });