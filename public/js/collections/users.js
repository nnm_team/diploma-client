define(['Backbone', 'models/user'],
    function (Backbone, UserModel) {

        return Backbone.Collection.extend({

            model: UserModel,

            url: 'verification'

        });

    });