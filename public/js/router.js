define(['Backbone', 'views/header'], function (Backbone, Header) {

    var unauthorisedViews = ['sign/signIn', 'sign/signUp', 'sign/forgotPassword'];

    return Backbone.Router.extend({

        routes: {
            ''              : 'root',
            'ccp'           : 'ccp',
            'main'          : 'main',
            'groups/:id'    : 'group',
            'signIn'        : 'signIn',
            'signUp'        : 'signUp',
            'groups'        : 'groups',
            'signOut'       : 'signOut',
            'teachers'      : 'teachers',
            'statistics'    : 'statistics',
            'verification'  : 'verification',
            'forgotPassword': 'forgotPassword'
        },

        initialize: function () {
            Backbone.history.start();
        },

        loadView: function (view, opts, cb) {
            var self = this;
            var isUnauthorised = !APP.user;
            var isUnauthorisedView = !!~unauthorisedViews.indexOf(view);

            if ((!isUnauthorised && isUnauthorisedView)
                || (isUnauthorised && !isUnauthorisedView)) {
                return Backbone.history.navigate('', {trigger: true});
            }

            if (isUnauthorisedView && self.header) {
                self.header.$el.html('');
                self.header.stopListening();
                self.header.undelegateEvents();
                self.header = null;
            } else if (!isUnauthorisedView && !self.header) {
                self.header = new Header();
            }

            require(['views/' + view], function (View) {
                if (self.view) {
                    self.view.$el.html('');
                    self.view.stopListening();
                    self.view.undelegateEvents();
                }

                self.view = new View(opts);

                typeof cb === 'function' && cb();
            });
        },

        root: function () {
            var route = APP.user ? 'main' : 'signIn';

            Backbone.history.navigate(route, {trigger: true})
        },

        signIn: function () {
            this.loadView('sign/signIn');
        },

        signUp: function () {
            this.loadView('sign/signUp');
        },

        forgotPassword: function () {
            this.loadView('sign/forgotPassword');
        },

        main: function () {
            this.loadView('hello');
        },

        ccp: function () {
            var access = APP.user && APP.user.access;

            if (access & 8) {
                return this.loadView('ccp');
            }

            this.root();
        },

        verification: function () {
            var access = APP.user.access;

            if (access & 8) {
                return this.loadView('verification/verification');
            }

            this.root();
        },

        teachers: function () {
            this.loadView('teachers/teachers');
        },

        groups: function () {
            this.loadView('groups/groups');
        },

        group: function (id) {
            this.loadView('groups/group', {id: id})
        },

        statistics: function () {
            this.loadView('statistics');
        }

    });
});