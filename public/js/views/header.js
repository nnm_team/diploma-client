define(['Backbone', 'jQuery', 'Underscore',
        'text!templates/header.html'],
    function (Backbone, $, _,
              template) {
        return Backbone.View.extend({

            el: 'header',

            template: _.template(template),

            events: {
                'click #onSignOut': 'onSignOut'
//                'click #profile_name': 'profile',
//                'click img': 'profile'
            },

            initialize: function () {
                this.render();
            },

            onSignOut: function () {
                $.ajax({
                    method: 'DELETE',
                    url   : 'auth/signout',
                    success   : function () {
                        APP.user = null;
                        Backbone.history.navigate('', {trigger: true});
                    },
                    error   : function () {
                        APP.user = null;
                        Backbone.history.navigate('', {trigger: true});
                    }
                });
            },

//            profile: function () {
//                var _id = this.model.get('_id');
//                profile(_id);
//            }

            render: function () {
                this.$el.html(this.template(APP.user));

                return this;
            }

        });
    });
