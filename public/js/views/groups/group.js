define(['Backbone', 'jQuery', 'Underscore', 'alertify',
        'models/group', 'models/student',
        'text!templates/groups/group.html'],
    function (Backbone, $, _, alertify,
              GroupModel, StudentModel,
              template) {
        return Backbone.View.extend({

            el: '#wrapper',

            template: _.template(template),

            events: {
                'click .fa-sort-numeric-asc': 'onCreateJob',
                'click .fa-minus-circle'    : 'onRemoveFromGroup'
            },

            initialize: function (opts) {
                opts || (opts = {});

                this.id = opts.id;

                this.group = new GroupModel({_id: this.id});

                this.group.on('sync', this.render, this);

                this.group.fetch();
            },

            onRemoveFromGroup: function (e) {
                var group = this.group;
                var $target = $(e.currentTarget);
                var id = $target.closest('tr').attr('data-id');

                $.ajax({
                    method : 'DELETE',
                    url    : 'students/' + id,
                    success: function () {
                        alertify.success('Student removed from group.');
                        group.fetch();
                    },
                    error  : function () {
                        group.fetch();
                    }
                });
            },

            onCreateJob: function (e) {
                var self = this;
                var group = this.group;
                var $target = $(e.currentTarget);
                var id = $target.closest('tr').attr('data-id');

                require(['views/groups/job'], function (View) {
                    if (self.jobView) {
                        self.jobView.$el.html('');
                        self.jobView.stopListening();
                        self.jobView.undelegateEvents();
                    }

                    self.jobView = new View();

                    self.jobView.on('createJob', function (data) {
                        self.jobView.$el.html('');
                        self.jobView.stopListening();
                        self.jobView.undelegateEvents();

                        $.ajax({
                            data   : data,
                            method : 'POST',
                            url    : 'jobs/student/' + id,
                            success: function () {
                                alertify.success('Created jobs.');
                                group.fetch();
                            },
                            error  : function () {
                                group.fetch();
                            }
                        });
                    });
                });
            },

            render: function () {
                var data = {
                    access: APP.user.access,
                    group : this.group.toJSON()
                };

                this.$el.html(this.template(data));
            }

        });
    });
