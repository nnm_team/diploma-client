define(['Backbone', 'jQuery', 'Underscore', 'alertify',
        'collections/groups',
        'text!templates/groups/groups.html'],
    function (Backbone, $, _, alertify,
              GroupCollection,
              template) {
        return Backbone.View.extend({
            el: '#wrapper',

            template: _.template(template),

            events: {
                'click .group-ctrl.fa-info'       : 'onGroup',
                'click .group-ctrl.fa-arrow-right': 'onUpCourse',
                'click .group-ctrl.fa-arrow-left' : 'onDownCourse'
            },

            initialize: function () {
                this.groups = new GroupCollection();

                this.groups.on('sync', this.render, this);

                this.groups.fetch();
            },

            onDownCourse: function (e) {
                var collection = this.groups;
                var $target = $(e.currentTarget);
                var id = $target.closest('.group').attr('data-id');
                var group = collection.get(id);
                var course = group.get('course');

                if (course <= 1) {
                    return;
                }

                group.save({course: --course}, {patch: true})
            },

            onUpCourse: function (e) {
                var collection = this.groups;
                var $target = $(e.currentTarget);
                var id = $target.closest('.group').attr('data-id');
                var group = collection.get(id);
                var course = group.get('course');

                if (course >= 6) {
                    return;
                }

                group.save({course: ++course}, {patch: true})

            },

            onGroup: function (e) {
                var $target = $(e.currentTarget);
                var id = $target.closest('.group').attr('data-id');

                Backbone.history.navigate('groups/' + id, {trigger: true});
            },

            render: function () {
                var data = {
                    access: APP.user.access,
                    groups: this.groups.toJSON()
                };

                this.$el.html(this.template(data));
            }
        });
    });
