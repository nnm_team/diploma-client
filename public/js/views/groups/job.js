define(['Backbone', 'jQuery', 'Underscore', 'alertify',
        'models/teacher',
        'text!templates/groups/job.html'],
    function (Backbone, $, _, alertify,
              TeacherModel,
              template) {

        return Backbone.View.extend({

            el: '#job_block',

            template: _.template(template),

            events: {
                'click #onJobCreate': 'onJobCreate'
            },

            initialize: function () {
                this.teacher = new TeacherModel({_id: APP.user._id});

                this.teacher.on('sync', this.render, this);

                this.teacher.fetch();
            },

            onJobCreate: function () {
                var data;
                var name = this.$el.find('#job_name').val();
                var rating = this.$el.find('#job_rating').val();
                var subject = this.$el.find('select').find('option:selected').attr('data-id');

                if (!name || !rating || !subject) {
                    alertify.error('Please type all fields.');
                }

                if (rating < 1 || rating > 100) {
                    alertify.error('Incorrect rating.');
                }

                data = {
                    name   : name,
                    rating : rating,
                    subject: subject
                };

                this.trigger('createJob', data);
            },

            render: function () {
                var data = {
                    teacher: this.teacher.toJSON()
                };

                this.$el.html(this.template(data));
            }

        });

    });
