define(['Backbone', 'jQuery', 'Underscore', 'alertify',
        'collections/users',
        'text!templates/verification/verification.html'],
    function (Backbone, $, _, alertify,
              UserCollection,
              template) {
        return Backbone.View.extend({

            el: '#wrapper',

            template: _.template(template),

            events: {
                'click .verification_delete' : 'onDelete',
                'click .verification_admin'  : 'onVerificationAdmin',
                'click .verification_student': 'onVerificationStudent',
                'click .verification_teacher': 'onVerificationTeacher'
            },

            initialize: function () {
                this.users = new UserCollection();

                this.users.on('sync', this.render, this);

                this.users.fetch();
            },

            loadSubView: function (id, subViewName) {
                var self = this;
                var collection = this.users;
//                verification_support
                require(['views/verification/support/' + subViewName], function (View) {
                    if (self.supportView) {
                        self.supportView.$el.html('');
                        self.supportView.stopListening();
                        self.supportView.undelegateEvents();
                    }

                    self.supportView = new View();

                    self.supportView.on('update/' + subViewName, function (data) {
                        self.supportView.$el.html('');
                        self.supportView.stopListening();
                        self.supportView.undelegateEvents();

                        $.ajax({
                            data   : data,
                            method : 'PATCH',
                            url    : 'verification/' + id + '/' + subViewName,
                            success: function () {
                                alertify.success('User was verificated.');
                                collection.fetch();
                            },
                            error  : function () {
                                collection.fetch();
                            }
                        });
                    });

                    self.supportView.on('update/error', function (message) {
                        self.supportView.$el.html('');
                        self.supportView.stopListening();
                        self.supportView.undelegateEvents();

                        alertify.error(message);
                    });
                });
            },

            onVerificationStudent: function (e) {
                var $target = $(e.currentTarget);
                var id = $target.closest('li').attr('data-id');

                this.loadSubView(id, 'student')
            },

            onVerificationTeacher: function (e) {
                var $target = $(e.currentTarget);
                var id = $target.closest('li').attr('data-id');

                this.loadSubView(id, 'teacher');
            },

            onVerificationAdmin: function (e) {
                var collection = this.users;
                var $target = $(e.currentTarget);
                var id = $target.closest('li').attr('data-id');

                $.ajax({
                    method : 'PATCH',
                    url    : 'verification/' + id + '/admin',
                    success: function () {
                        alertify.success('User was verificated.');
                        collection.fetch();
                    },
                    error  : function () {
                        collection.fetch();
                    }
                });
            },

            onDelete: function (e) {
                var collection = this.users;
                var $target = $(e.currentTarget);
                var id = $target.closest('li').attr('data-id');

                $.ajax({
                    method : 'DELETE',
                    url    : 'verification/' + id + '/delete',
                    success: function () {
                        alertify.success('User was deleted.');
                        collection.fetch();
                    },
                    error  : function () {
                        collection.fetch();
                    }
                });
            },

            render: function () {
                var data = {users: this.users.toJSON()};

                this.$el.html(this.template(data));

                this.$subView = this.$el.find('#verification_support');
            }

        });
    });