define(['Backbone', 'jQuery', 'Underscore', 'alertify',
        'collections/groups',
        'text!templates/verification/student.html'],
    function (Backbone, $, _, alertify,
              GroupCollection,
              template) {
        return Backbone.View.extend({

            el: '#verification_support',

            template: _.template(template),

            events: {
                'click #onStudentVerification': 'onStudentVerification'
            },

            initialize: function () {
                this.groups = new GroupCollection();

                this.groups.on('sync', this.render, this);

                this.groups.fetch();
            },

            onStudentVerification: function () {
                var group = this.$list.find('option:selected').attr('data-id');
                var data = {group: group};

                this.trigger('update/student', data);
            },

            render: function () {
                var data;

                if (!this.groups.length) {
                    return this.trigger('update/error', 'Do not have enough groups.');
                }

                data = {groups: this.groups.toJSON()};

                this.$el.html(this.template(data));

                this.$list = this.$el.find('select');
            }
        });
    });