define(['Backbone', 'jQuery', 'Underscore', 'alertify',
        'collections/subjects',
        'text!templates/verification/teacher.html', 'text!templates/verification/subject.html'],
    function (Backbone, $, _, alertify,
              SubjectCollection,
              template, subjectTemplate) {
        return Backbone.View.extend({

            el: '#verification_support',

            template       : _.template(template),
            subjectTemplate: _.template(subjectTemplate),

            events: {
                'click #add_subject'          : 'onAddSubject',
                'click .remove'               : 'onRemoveSubject',
                'click #onTeacherVerification': 'onTeacherVerification'
            },

            initialize: function () {
                this.activeSubjects = 0;

                this.subjects = new SubjectCollection();

                this.subjects.on('sync', this.render, this);

                this.subjects.fetch();
            },

            onAddSubject: function () {
                var data;

                if (this.activeSubjects >= this.subjects.length) {
                    return;
                }

                this.activeSubjects++;

                data = {
                    subjects: this.subjects.toJSON()
                };

                this.$list.append(this.subjectTemplate(data));
            },

            onRemoveSubject: function (e) {
                $(e.currentTarget)
                    .closest('.subject')
                    .remove();

                this.activeSubjects--;
            },

            onTeacherVerification: function () {
                var data;
                var subjects = [];
                var $lists = this.$list.find('.subject select');

                _.each($lists, function (list) {
                    var id = $(list)
                        .find('option:selected')
                        .attr('data-id');

                    subjects.push(id);
                });

                data = {subjects: subjects};

                this.trigger('update/teacher', data);
            },

            render: function () {
                var data;

                if (!this.subjects.length) {
                    return this.trigger('update/error', 'Do not have enough subjects.');
                }

                data = {subjects: this.subjects.toJSON()};

                this.$el.html(this.template(data));

                this.$list = this.$el.find('#subject_list');
            }
        });
    });