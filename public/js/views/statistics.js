define(['Backbone', 'jQuery', 'underscore', 'alertify', 'async',
        'text!templates/statistics.html',
        'helpers/draw'],
    function (Backbone, $, _, alertify, async,
              template,
              draw) {
        return Backbone.View.extend({

            el: '#wrapper',

            template: _.template(template),

            initialize: function () {
                var self = this;

                this.statistics = {};

                async.parallel({

                    groups: function (pCb) {
                        $.ajax({
                            method : 'GET',
                            url    : 'statistics/groups',
                            success: function (xhr) {
                                pCb(null, xhr);
                            },
                            error  : function (xhr) {
                                pCb(xhr);
                            }
                        })
                    },

                    courses: function (pCb) {
                        $.ajax({
                            method : 'GET',
                            url    : 'statistics/courses',
                            success: function (xhr) {
                                pCb(null, xhr);
                            },
                            error  : function (xhr) {
                                pCb(xhr);
                            }
                        })
                    },

                    subjects: function (pCb) {
                        $.ajax({
                            method : 'GET',
                            url    : 'statistics/subjects',
                            success: function (xhr) {
                                pCb(null, xhr);
                            },
                            error  : function (xhr) {
                                pCb(xhr);
                            }
                        })
                    }

                }, function (err, result) {
                    if (err) {
                        alertify.error('Get statistics error.');
                        return Backbone.history.navigate('', {trigger: true});
                    }

                    console.log(result);

                    self.statistics.groups = result.groups;
                    self.statistics.courses = result.courses;
                    self.statistics.subjects = result.subjects;

                    self.render();
                });
            },

            render: function () {
                this.$el.html(template);

                draw('.chart_group', this.statistics.groups);
                draw('.chart_courses', this.statistics.courses);
                draw('.chart_subject', this.statistics.subjects);

                return this;
            }
        });
    });