define(['Backbone',
        'text!templates/hello.html'],
    function (Backbone,
              template) {
        return Backbone.View.extend({

            el: '#wrapper',

            initialize: function () {
                return this.render();
            },

            render: function () {
                this.$el.html(template);

                return this;
            }

        });
    });