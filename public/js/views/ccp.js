define(['Backbone', 'jQuery', 'Underscore', 'async', 'alertify',
        'models/group', 'models/subject',
        'collections/groups', 'collections/subjects',
        'text!templates/ccp/ccp.html', 'text!templates/ccp/groups.html', 'text!templates/ccp/subjects.html'],
    function (Backbone, $, _, async, alertify,
              GroupModel, SubjectModel,
              GroupCollection, SubjectCollection,
              template, groupsTemplate, subjectTemplate) {
        return Backbone.View.extend({

            el: '#wrapper',

            template       : _.template(template),
            groupsTemplate : _.template(groupsTemplate),
            subjectTemplate: _.template(subjectTemplate),

            events: {
                'click span.group.fa.fa-pencil'  : 'onEditGroup',
                'click span.group.fa.fa-trash'   : 'onDeleteGroup',
                'click #onCreateGroup'           : 'onCreateGroup',
                'click span.subject.fa.fa-pencil': 'onEditSubject',
                'click span.subject.fa.fa-trash' : 'onDeleteSubject',
                'click #onCreateSubject'         : 'onCreateSubject'
            },

            initialize: function () {
                this.render();

                this.groups = new GroupCollection();
                this.subjects = new SubjectCollection();

                this.groups.on('sync', this.renderGroups, this);
                this.subjects.on('sync', this.renderSubjects, this);

                this.groups.fetch();
                this.subjects.fetch();
            },

            onCreateGroup: function () {
                var group;
                var collection = this.groups;
                var groupName = this.$groupName.val();

                if (!groupName) {
                    return alertify.error('Please type valid group name!');
                }

                group = new GroupModel({course: 1, name: groupName});

                group.save(null, {
                    success: function () {
                        alertify.success('Group was create successfully.');
                        collection.fetch();
                    },
                    error  : function (mod, xhr) {
                        alertify.error(xhr.responseJSON.error);
                    }
                });
            },

            onEditGroup: function (e) {
                var name;
                var newName;
                var collection = this.groups;
                var $target = $(e.currentTarget);
                var id = $target.closest('li').attr('data-id');
                var group = collection.get(id);

                if (!group) {
                    return;
                }

                name = group.get('name');
                newName = prompt('Type new name: ', name);

                group.save({name: newName}, {
                    patch: true,

                    success: function () {
                        alertify.success('Group was update successfully.');
                        collection.fetch();
                    },

                    error: function (mod, xhr) {
                        collection.fetch();
                        alertify.error(xhr.responseJSON.error);
                    }
                });
            },

            onDeleteGroup: function (e) {
                var collection = this.groups;
                var $target = $(e.currentTarget);
                var id = $target.closest('li').attr('data-id');
                var group = collection.get(id);

                if (!group) {
                    return;
                }

                group.destroy({
                    success: function () {
                        alertify.success('Group was delete successfully.');
                        collection.fetch();
                    },
                    error  : function (mod, xhr) {
                        collection.fetch();
                        alertify.error(xhr.responseJSON.error);
                    }
                });
            },

            onCreateSubject: function () {
                var subject;
                var collection = this.subjects;
                var subjectName = this.$subjectName.val();

                if (!subjectName) {
                    return alertify.error('Please type valid group name!');
                }

                subject = new SubjectModel({course: 1, name: subjectName});

                subject.save(null, {
                    success: function () {
                        alertify.success('Subject was create successfully.');
                        collection.fetch();
                    },
                    error  : function (mod, xhr) {
                        alertify.error(xhr.responseJSON.error);
                    }
                });
            },

            onEditSubject: function (e) {
                var name;
                var newName;
                var collection = this.subjects;
                var $target = $(e.currentTarget);
                var id = $target.closest('li').attr('data-id');
                var subject = collection.get(id);

                if (!subject) {
                    return;
                }

                name = subject.get('name');
                newName = prompt('Type new name: ', name);

                subject.save({name: newName}, {
                    patch: true,

                    success: function () {
                        alertify.success('Subject was update successfully.');
                        collection.fetch();
                    },

                    error: function (mod, xhr) {
                        collection.fetch();
                        alertify.error(xhr.responseJSON.error);
                    }
                });
            },

            onDeleteSubject: function (e) {
                var collection = this.subjects;
                var $target = $(e.currentTarget);
                var id = $target.closest('li').attr('data-id');
                var subject = collection.get(id);

                if (!subject) {
                    return;
                }

                subject.destroy({
                    success: function () {
                        alertify.success('Group was delete successfully.');
                        collection.fetch();
                    },
                    error  : function (mod, xhr) {
                        collection.fetch();
                        alertify.error(xhr.responseJSON.error);
                    }
                });
            },

            renderSubjects: function () {
                var data = {
                    subjects: this.subjects.toJSON()
                };

                this.$subjectsBlock.html(this.subjectTemplate(data));

                this.$subjectName = this.$subjectsBlock.find('#newSubjectName');
            },

            renderGroups: function () {
                var data = {
                    groups: this.groups.toJSON()
                };

                this.$groupsBlock.html(this.groupsTemplate(data));

                this.$groupName = this.$groupsBlock.find('#newGroupName');
            },

            render: function () {
                this.$el.html(this.template);

                this.$groupsBlock = this.$el.find('#mg');
                this.$subjectsBlock = this.$el.find('#ms');

                return this;
            }
        });
    });