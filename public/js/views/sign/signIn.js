define(['Backbone', 'jQuery', 'alertify',
        'text!templates/sign/signIn.html'],
    function (Backbone, $, alertify,
              template) {

        return Backbone.View.extend({
            el: '#wrapper',

            events: {
                'click #onSignIn': 'onSignIn'
            },

            initialize: function () {
                this.render();
            },

            onSignIn: function () {
                var data;
                var mail = this.$el.find('#mail').val();
                var password = this.$el.find('#password').val();

                if (!mail || !password) {
                    return alertify.error('Type all required fields!');
                }

                if (password.length < 6) {
                    return alertify.error('Password is incorrect!');
                }

                data = {
                    mail    : mail,
                    password: password
                };

                $.ajax({
                    method : "POST",
                    url    : '/auth/signin',
                    data   : data,
                    success: function (xhr) {
                        APP.user = xhr;

                        Backbone.history.navigate('main', {trigger: true});

                        alertify.success('User was sign in successfully.');
                    },
                    error  : function () {
                        alertify.error('Sign in error.');
                    }
                });
            },

            render: function () {
                this.$el.html(template);

                return this;
            }
        });
    });