define(['Backbone', 'jQuery', 'alertify',
        'text!templates/sign/signUp.html'],
    function (Backbone, $, alertify,
              template) {

        return Backbone.View.extend({
            el: '#wrapper',

            events: {
                'click #onSignUp': 'onSignUp'
            },

            initialize: function () {
                this.render();
            },

            onSignUp: function (e) {
                var fd = new FormData();

                var name = this.$el.find('#name').val();
                var date = this.$el.find('#date').val();
                var mail = this.$el.find('#mail').val();
                var phone = this.$el.find('#phone').val();
                var password = this.$el.find('#password').val();
                var secretQuestion = this.$el.find('#secretQuestion').val();

                var files = this.$el.find('#fileInput')[0].files;
                var file = files && files[0];

                if (!name || !mail || !password || !secretQuestion) {
                    return alertify.error('Type all required fields!');
                }

                fd.append('name', name);
                fd.append('date', date);
                fd.append('mail', mail);
                fd.append('phone', phone);
                fd.append('password', password);
                fd.append('secretQuestion', secretQuestion);

                if (file) {
                    fd.append('avatar', file);
                }

                $.ajax({
                    method     : 'POST',
                    url        : '/auth/signup',
                    data       : fd,
                    contentType: false,
                    processData: false,
                    success    : function () {
                        Backbone.history.navigate('signIn', {trigger: true});

                        alertify.success('User was sign up successfully.');
                    },
                    error      : function () {
                        alertify.error('Sign up error.');
                    }
                });
            },

            render: function () {
                this.$el.html(template);

                return this;
            }
        });

    });