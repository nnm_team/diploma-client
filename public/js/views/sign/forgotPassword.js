define(['Backbone', 'jQuery', 'alertify',
        'text!templates/sign/forgotPassword.html'],
    function (Backbone, $, alertify,
              template) {
        return Backbone.View.extend({
            el: '#wrapper',

            events: {
                'click #onForgotPassword': 'onForgotPassword'
            },

            initialize: function () {
                this.render();
            },

            onForgotPassword: function () {
                var data;
                var mail = this.$el.find('#mail').val();
                var secretQuestion = this.$el.find('#secretQuestion').val();

                if (!mail || !secretQuestion) {
                    return alertify.error('Type all required fields!');
                }

                data = {
                    mail    : mail,
                    secretQuestion: secretQuestion
                };

                $.ajax({
                    method : "POST",
                    url    : '/auth/forgotPassword',
                    data   : data,
                    success: function () {
                        Backbone.history.navigate('signIn', {trigger: true});

                        alertify.success('Password was send to mail.');
                    },
                    error  : function () {
                        alertify.error('Incorrect secret question.');
                    }
                });
            },

            render: function () {
                this.$el.html(template);

                return this;
            }
        });
    });