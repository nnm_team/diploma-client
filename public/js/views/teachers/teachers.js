define(['Backbone', 'jQuery', 'Underscore', 'alertify',
        'collections/teachers',
        'text!templates/teachers/teachers.html'],
    function (Backbone, $, _, alertify,
              TeacherCollection,
              template) {

        return Backbone.View.extend({

            el: '#wrapper',

            template: _.template(template),

            events: {
                'click span.ctrl.fa.fa-trash-o': 'onDelete',
                'click span.ctrl.fa.fa-pencil' : 'onUpdateSubject'
            },

            initialize: function () {
                this.teachers = new TeacherCollection();

                this.teachers.on('sync', this.render, this);

                this.teachers.fetch();
            },

            loadSubView: function (id) {
                var self = this;
                var collection = this.teachers;

                require(['views/teachers/editSubjects'], function (View) {
                    if (self.editSubjectsView) {
                        self.editSubjectsView.$el.html('');
                        self.editSubjectsView.stopListening();
                        self.editSubjectsView.undelegateEvents();
                    }

                    self.editSubjectsView = new View();

                    self.editSubjectsView.on('update/teacher', function (data) {
                        var model;

                        self.editSubjectsView.$el.html('');
                        self.editSubjectsView.stopListening();
                        self.editSubjectsView.undelegateEvents();

                        model = collection.get(id);

                        model.save(data, {
                            patch  : true,
                            success: function () {
                                alertify.success('Teacher updated.');
                                collection.fetch();
                            },
                            error  : function () {
                                collection.fetch();
                            }
                        });
                    });
                });
            },

            onUpdateSubject: function (e) {
                var $target = $(e.target);
                var id = $target.closest('.teacher-item').attr('data-id');

                this.loadSubView(id);
            },

            onDelete: function (e) {
                var collection = this.teachers;
                var $target = $(e.currentTarget);
                var id = $target.closest('.teacher-item').attr('data-id');
                var teacher = collection.get(id);

                teacher.destroy({
                    success: function () {
                        alertify.success('Teacher was deleted.');
                        collection.fetch();
                    },
                    error  : function () {
                        collection.fetch();
                    }
                });
            },

            render: function () {
                var data = {
                    access  : APP.user.access,
                    teachers: this.teachers.toJSON()
                };

                this.$el.html(this.template(data));
            }

        });

    });
