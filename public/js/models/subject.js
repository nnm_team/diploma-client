define(['Backbone'], function (Backbone) {
    return Backbone.Model.extend({

        idAttribute: '_id',

        urlRoot: 'subjects'

    });
});