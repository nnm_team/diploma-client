define(['Backbone', '../const'], function (Backbone, C) {

    return Backbone.Model.extend({

        idAttribute: '_id'

    });

});